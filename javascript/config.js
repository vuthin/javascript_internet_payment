tailwind.config = {
    theme: {
      extend: {
        colors: {
          clifford: '#da373d',
        },
        fontFamily: {
          Poppin: ['Poppins, sans-serif']
      },
      height: {
          He: '560px',
      },
        
      }
    }
  }
  