let show = document.getElementById("show");
setInterval(function () {
  let localTime = new Date();
  show.innerText =
    localTime.toDateString() + " || " + localTime.toLocaleTimeString();
}, 1000);
var dateStart;
var dateStop;
// Catch Start time
var btnCus = document.getElementById("btn");

function btnStart() {
  dateStart = new Date(Date.now());
  btnCus.className =
    " w-96 bg-red-500 text-white font-bold py-2 px-4 border border-blue-700 rounded text-4xl";
  btnCus.innerHTML = "stop";
  document.getElementById("start").innerHTML = dateStart.toLocaleTimeString();
}

// Catch Stope time
function btnStop() {
  dateStop = new Date(Date.now());
  btnCus.className =
    " w-96 bg-red-400 text-white font-bold py-2 px-4 border border-blue-700 rounded text-4xl";
  btnCus.innerHTML = "clear";
  document.getElementById("stop").innerHTML = dateStop.toLocaleTimeString();
  // Minute
  let tmin = Math.trunc((dateStop - dateStart) / 60000);
  let total = 1;
  document.getElementById("nimute").innerHTML = tmin;

  if (tmin <= 15) {
    document.getElementById("total").innerHTML = "500(riel)";
  } else if (tmin >= 16 && tmin <= 30) {
    document.getElementById("total").innerHTML = " 1000(riel)";
  } else if (tmin > 30 && tmin <= 60) {
    document.getElementById("total").innerHTML = " 1500(riel)";
  } else {
    document.getElementById("total").innerHTML =
      parseInt((tmin / 60) * 1500) + 500;
  }
}

// Clear time
function btnClearTime() {
  var date = new Date(Date.now());
  btnCus.className =
    "w-96 bg-yellow-900 text-white font-bold py-2 px-4 border border-blue-700 rounded text-4xl";
  btnCus.innerHTML = "start";
  document.getElementById("start").innerHTML = "0:00";
  document.getElementById("stop").innerHTML = "0:00";
  document.getElementById("nimute").innerHTML = "0:00";
  document.getElementById("total").innerHTML = "0:00";
  
}

var isBtnStart = true;
var isBtnStop = false;
var isBtnClear = false;

var startTime = document.getElementById("start");
var button = document.getElementById("btnStart");

function btnClick() {
  if (isBtnStart == true) {
    btnStart();
    isBtnStart = false;
    isBtnStop = true;
    isBtnClear = false;
  } else if (isBtnStop) {
    btnStop();
    isBtnStart = false;
    isBtnStop = false;
    isBtnClear = true;
  } else {
    btnClearTime();
    isBtnStart = true;
    isBtnStop = false;
    isBtnClear = false;
  }
}
